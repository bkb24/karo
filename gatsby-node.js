const path = require('path')
const { fmImagesToRelative } = require('gatsby-remark-relative-images');

// module.exports.onCreateNode = ({ node, actions, getNode }) => {
//     // const { createNodeField } = actions
//     // fmImagesToRelative(node) // convert image paths for gatsby images

//     // const { createNodeField } = actions

//     // if (node.internal.type === 'MarkdownRemark') {
//     //     const slug = path.basename(node.fileAbsolutePath, '.md')

//     //     createNodeField({
//     //         node,
//     //         name: 'slug',
//     //         value: slug
//     //     })
//     // }
// }

exports.onCreateNode = ({ node, actions }) => {
    const { createNodeField } = actions
    fmImagesToRelative(node) // convert image paths for gatsby images

    if (node.internal.type === 'MarkdownRemark') {
        const slug = path.basename(node.fileAbsolutePath, '.md')

        createNodeField({
            node,
            name: 'slug',
            value: slug
        })
    }

    // if (node.internal.type === `MarkdownRemark`) {
    //   const value = createFilePath({ node, getNode })
    //   createNodeField({
    //     name: `slug`,
    //     node,
    //     value,
    //   })
    // }
}

// module.exports.createPages = ({ graphql, actions }) => {
//     // const { createPage } = actions
// }

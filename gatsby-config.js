/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

const path = require('path');

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

var netlifyCmsPaths = {
  resolve: `gatsby-plugin-netlify-cms-paths`,
  options: {
    cmsConfig: `/static/admin/config.yml`,
  },
}

module.exports = {
  siteMetadata: {
    title: `Пицария Каро Варна Трошево`,
    siteUrl: `https://www.gatsbyjs.org`,
    description: `Заповядайте в пицария Каро М Варна, кв. Трошево.
    Поръчайте пица, на парче или цяла, храна за тук или за вкъщи, супи, десерти, спагети...
    Организираме частни и фирмени празненства и мероприятия - обадете се на тел: 052 / 804 404.
    `
  },
  /* Your site config here */
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/assets/`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/assets/images`,
        name: `images`,
      },
    },
    'gatsby-plugin-sass',
    `gatsby-plugin-react-helmet`,
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: `${__dirname}/src/pages`
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: `${__dirname}/src/pages/menu`
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'lunch',
        path: `${__dirname}/src/lunch-menu/`
      }
    },
    `gatsby-source-instance-name-for-remark`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    netlifyCmsPaths,
    {
      // netlifyCmsPaths,
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `gatsby-remark-relative-images`,
          {
            resolve: `gatsby-remark-images`,
            options: {},
          },
        ],
      },
    },
    // {
    //   resolve: 'gatsby-transformer-remark',
    //   options: {
    //     plugins: [
    //       netlifyCmsPaths,
    //       // {
    //       //   resolve: 'gatsby-remark-relative-images'
    //       // },
    //       {
    //         resolve: 'gatsby-remark-images',
    //         options: {
    //           // It's important to specify the maxWidth (in pixels) of
    //           // the content container as this plugin uses this as the
    //           // base for generating different widths of each image.
    //           maxWidth: 2048,
    //           backgroundColor: 'transparent', // required to display blurred image first
    //         },
    //       }
    //     ],
    //   },
    // },
    `gatsby-plugin-playground`,
    `gatsby-plugin-netlify-cms`,
  ]
}

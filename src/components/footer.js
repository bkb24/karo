import React from 'react'
import { Link } from "gatsby"
import logo from "../assets/images/icons/logo.svg"

const Footer = () => {
    return (
        <footer className="karo-footer">

            <h2 className="footer-head">Каро М</h2>

            <div className="footer-col footer-left">
                <div className="footer-section footer-menu">
                    <h3 className="footer-heading">Меню</h3>

                    <ul className="footer-list">
                        <li className="footer-list-item">Пица</li>
                        <li className="footer-list-item">Салати</li>
                        <li className="footer-list-item">Супи</li>
                        <li className="footer-list-item">Десерти</li>
                    </ul>
                </div>
            </div>

            <div className="footer-middle">
                <img src={logo} />
            </div>

            <div className="footer-col footer-right">
                <div className="footer-section footer-contacts">
                    <h3 className="footer-heading">Контакти</h3>

                    <ul className="footer-list">
                        <li className="footer-list-item">
                            <span className="icon i-location"></span>
                            <span>Варна, кв. Трошево, ул. 'Ивац' 11</span>
                        </li>
                        <li className="footer-list-item">
                            <span className="icon i-phone"></span>
                            <span>052 / 804 404</span>
                        </li>
                        <li className="footer-list-item">
                            <span className="icon i-location"></span>
                            <span>facebook</span>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="footer-copy">Пицария Каро М {(new Date()).getFullYear()}</div>
        </footer>
    )
}

export default Footer

import React from "react"

import Seo from "./seo"
import Header from "./header"
import Footer from "./footer"

import "../assets/scss/main.scss"

const Layout = (data) => {
    let className = data.className || '';

    return (
        <div>
          {/* <Seo /> */}
          <Header className={className} />
          {data.children}
          <Footer />
        </div>
    )
}

export default Layout;

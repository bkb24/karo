import React from 'react'
import { graphql, StaticQuery } from 'gatsby'

import styled from 'styled-components'
import BackgroundImage from 'gatsby-background-image'

const MenuHero = () => (
    <StaticQuery query={graphql`
            {
                file(
                    relativePath: { eq: "menu.jpg" }
                ) {
                    id
                    absolutePath
                    childImageSharp {
                        # Specify the image processing specifications right in the query.
                        # Makes it trivial to update as your page's design changes.
                        fluid(maxWidth: 4896) {
                            ...GatsbyImageSharpFluid
                        }
                    }
                }
            }
        `}
        render={ data => {
                const imageData = data.file.childImageSharp.fluid

                return (
                    <BackgroundImage Tag="section" className="menu-hero" fluid={imageData} />
                )
            }
        }
    />
)

const StyledBackgroundSection = styled(MenuHero)`
  width: 100%;
  background-position: initial;
  background-repeat: no-repeat;
  background-size: 100% auto;
`

export default StyledBackgroundSection

import React from 'react'
import { Link, graphql, StaticQuery } from "gatsby";
import styled from 'styled-components'
import BackgroundImage from 'gatsby-background-image'

const HomeHero = () => (
    <StaticQuery query={graphql`
            {
                file(
                    relativePath: { eq: "home-hero-bg.jpg" }
                ) {
                    id
                    absolutePath
                    childImageSharp {
                        # Specify the image processing specifications right in the query.
                        # Makes it trivial to update as your page's design changes.
                        fluid(maxWidth: 4160) {
                            ...GatsbyImageSharpFluid
                        }
                    }
                }
            }
        `}
        render={ data => {
                const imageData = data.file.childImageSharp.fluid

                return (
                    <BackgroundImage Tag="section" className="home-hero" fluid={imageData} >
                        <div className="home-hero-content">

                        <h2 className="home-hero-greeting">Заповядайте!</h2>

                        <h1 className="home-hero-heading">Пицария <span style={{ color: "rgb(207, 0, 0)" }}>Каро</span> М</h1>

                        <div className="home-hero-description">
                            <p className="home-hero-description-item">
                                Пица, вкусни ястия за тук или за вкъщи, десерти...
                            </p>

                            <p className="home-hero-description-item">
                                Организираме частни и фирмени празненства, обадете ни се!
                            </p>
                        </div>

                        <div className="home-hero-contacts">
                            <div className="home-hero-address">
                                <span className="icon i-location"></span>
                                <span>Варна, кв. Трошево, ул. 'Ивац' 11</span>
                            </div>
                            <div className="home-hero-phone">
                                <span className="icon i-phone"></span>
                                <span>052 / 804 404</span>
                            </div>
                        </div>

                        <div className="home-hero-cta">
                            <a href="#map" className="btn btn-hero">Виж на картата</a>
                            <Link to="/меню" className="btn btn-hero">Пълно меню</Link>
                        </div>
                        </div>

                        <div className="home-hero-to-slide">
                        Обедно меню
                        </div>
                    </BackgroundImage>
                 )
            }
        }
    />
)

const StyledBackgroundSection = styled(HomeHero)`
  width: 100%;
  background-position: initial;
  background-repeat: no-repeat;
  background-size: 100% auto;
`

export default StyledBackgroundSection

import React from 'react'
import { Link } from "gatsby"

const Header = (data) => {
    let className = `karo-header ${(data.className ? data.className : '')}`;

    return (
        <header className={className}>
            <nav className="karo-main-nav">
                <ul className="nav-list karo-main-nav-list">
                    <li className="nav-li karo-main-nav-li">
                        <Link className="main-nav-link" to="/">Начало</Link>
                    </li>
                    <li className="nav-li karo-main-nav-li">
                        <Link className="main-nav-link" to="/меню">Меню</Link>
                        <ul className="nav-list nav-list-sub">
                            <li className="nav-li">
                                <Link className="main-nav-link" to="/меню">Меню</Link>
                            </li>
                            <li className="nav-li">
                                <Link className="main-nav-link" to="/обедно-меню">Обедно Меню</Link>
                            </li>
                        </ul>
                    </li>
                    <li className="nav-li karo-main-nav-li">
                        <Link className="main-nav-link" to="/галерия">Галерия</Link>
                    </li>
                    <li className="nav-li karo-main-nav-li">
                        <Link className="main-nav-link" to="/контакти">Контакти</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

export default Header

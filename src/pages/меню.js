import React from 'react'
import Layout from '../components/layout'
import { graphql } from 'gatsby'

import MenuHero from '../components/partials/menu-hero'
import logo from "../assets/images/icons/logo.svg"

const MenuPage = ({ data }) => {

    let menu = data ? data.allMarkdownRemark.edges[0].node.frontmatter.type : null;

    return (
        <Layout>
            <MenuHero />
            <div className="karo-menu karo-full-menu">
                <img src={logo} />

                <h1 className="menu-main-heading">Меню</h1>
                { menu && menu.map((type, idx) => {
                    return (
                        <div className="menu-section" key={idx}>
                            <h2 className="menu-section-name">{type.title}</h2>

                            <div className="menu-foods">
                                {type && type.foods.map((food, index) => {
                                return <div className="home-lunch-menu-item" key={index}>
                                    <div className="home-lunch-menu-food">
                                    <div className="home-lunch-menu-food-name">{food.title}</div>
                                    {/* <div className="home-lunch-menu-allergens">алергени: {food.allergens}</div> */}
                                    </div>

                                    <div className="home-lunch-menu-quantity">{food.quantity} {food.quantityUnits} / {food.price}лв</div>
                                </div>
                                })}
                            </div>
                        </div>
                    )})

                }

            </div>
        </Layout>
    )
}

export default MenuPage

export const menuQuery = graphql`
    query menuItemsQuery {
        allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/pages\/menu\/menu.md/" } }
        ) {
            edges {
                node {
                    frontmatter {
                        type {
                            title
                            foods {
                                title
                                quantity
                                quantityUnits
                                price
                            }
                        }
                    }

                }
            }
        }
    }
`

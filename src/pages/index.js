import React from 'react'
import { graphql, useStaticQuery } from "gatsby";
import moment from 'moment'

import Layout from '../components/layout'
import HomeHero from '../components/partials/home-hero'
import logo from "../assets/images/icons/logo.svg"

const IndexPage = () => {
    const data = useStaticQuery(graphql`
        {
            allMarkdownRemark(
                filter: { fileAbsolutePath: { regex: "//lunch-menu//" } }
            ) {
                edges {
                    node {
                        fileAbsolutePath
                        frontmatter {
                            foods {
                                allergens
                                name
                                quantity
                                quantityUnits
                            }
                            date
                        }
                    }
                }
            }
        }
    `)

    let dayWeekShort = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
    let todayMenuIndex = (new Date()).getDay();
    let todaySlug = (todayMenuIndex == 0 || todayMenuIndex == 6) ? dayWeekShort[5] : dayWeekShort[todayMenuIndex];

    let todayMenu = data.allMarkdownRemark.edges.find(day => {
        return day.node.fileAbsolutePath.endsWith(`${todaySlug}.md`);
    });

    let menu = todayMenu ? todayMenu.node.frontmatter : null;
    let dayBg = ["Петък", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Петък"][todayMenuIndex];

    let date = moment();
    let weekStart = date.startOf('isoWeek').date();
    let weekEnd = date.endOf('isoWeek').date();
    let month = date.format('MM')
    let weekDates = `${weekStart}.${month} - ${weekEnd}.${month}`;

    return (
        <Layout className="home-header">
            <HomeHero />

            <div className="home-lunch-wrap">
                <div className="karo-menu home-lunch-menu">
                    <img src={logo} />

                    <h2 className="home-lunch-menu-main-heading">
                        Обедно меню
                        <span className="home-lunch-menu-day home-lunch-menu-week">{weekDates}</span>
                    </h2>

                    <h3 className="home-lunch-menu-sub-heading">
                        {dayBg}
                        <span className="home-lunch-menu-day home-lunch-menu-today">{moment().date()}.{month}</span>
                    </h3>

                    <div className="home-lunch-menu-contents">
                        {menu && menu.foods && menu.foods.map((food, index) => {
                        return <div className="home-lunch-menu-item" key={index}>
                                <div className="home-lunch-menu-food">
                                    <div className="home-lunch-menu-food-name">{food.name}</div>
                                    <div className="home-lunch-menu-allergens">алергени: {food.allergens}</div>
                                </div>

                                <div className="home-lunch-menu-quantity">{food.quantity} {food.quantityUnits}</div>
                            </div>
                        })}
                    </div>

                    <div className="home-lunch-menu-end"></div>
                </div>
            </div>

            <div id="map" className="home-map-section">
                <h2 className="home-map-heading">Заповядайте на място</h2>
                <iframe className="home-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d264.50974379495926!2d27.88240231629019!3d43.22221974865724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40a45485276602b5%3A0xb0c797021e1707b2!2z0YPQuy4g4oCe0JjQstCw0YYg0JLQvtC50LLQvtC00LDigJwgMTEsIDkwMDkg0LYu0LouINCi0YDQvtGI0LXQstC-LCDQktCw0YDQvdCw!5e0!3m2!1sbg!2sbg!4v1560177414465!5m2!1sbg!2sbg" allowFullScreen></iframe>
            </div>
        </Layout>
    )
}

export default IndexPage

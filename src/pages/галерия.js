import React from 'react'
import { Link, graphql, useStaticQuery } from "gatsby";
import Layout from '../components/layout'

import Img from 'gatsby-image'

const Gallery = ({ data }) => {

    console.log('data', data)
    return (
        <Layout>
            <div className="gallery-wrap">
                {/* <div className="gallery-logo"></div> */}
                <h1 className="gallery-heading">Галерия</h1>

                <div className="gallery">
                    {data && data.allFile.edges.map((img, index) => {
                        return <div key={index} className="gallery-image">
                                <Img fluid={img.node.childImageSharp.fluid} />
                            </div>
                    })}
                </div>
            </div>
        </Layout>
    )
}

export default Gallery

export const galleryImages = graphql`
    query galleryQuery {
        allFile(filter: { absolutePath: { regex: "/static\/assets/" } }) {
            edges {
                node {
                    childImageSharp {
                        fluid(maxWidth: 1600) {
                            ...GatsbyImageSharpFluid
                        }
                    }
                }
            }
        }
    }
`
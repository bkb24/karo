---
date: 2019-06-07T14:00:42.254Z
foods:
  - allergens: нема
    name: Пица неква
    quantity: '1'
    quantityUnits: бр
  - allergens: нема
    name: Салата
    quantity: '200'
    quantityUnits: гр
  - allergens: много
    name: Кюфте
    quantity: '2'
    quantityUnits: бр
  - allergens: нема
    name: Леб
    quantity: '1'
    quantityUnits: бр
---

